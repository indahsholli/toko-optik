<h2>Tambah Produk</h2>

<form method="post" enctype="multipart/form-data">
	<div class="form-group">
		<label>nama</label>
		<input type="text" class="form-control" name="nama">
	</div>
	<div class="form-group">
		<label>Harga (Rp)</label>
		<input type="number" class="form-control" name="harga">
	</div>
	<div class="form-group">
		<label>Berat (Gr)</label>
		<input type="number" class="form-control" name="berat">
	</div>
	<div class="form-group">
		<label>Deskripsi</label>
		<textarea class="form-control" name="deskripsi" rows="10"></textarea>
	</div>
	<div class="form-group">
		<label>Foto</label>
		<input type="file" class="form-control" name="foto">
	</div>
	<button class="btn btn-primary" name="save">Simpan</button>
</form>
<?php
if (isset($_POST['save']))
{	$namaproduk = $_POST['nama'];
	$harga = $_POST['harga'];
	$berat = $_POST['berat'];
	$deskripsi = $_POST['deskripsi'];
	
	$nama = $_FILES['foto']['name'];
	$lokasi = $_FILES['foto']['tmp_name'];
	move_uploaded_file($lokasi, "../foto_produk/".$nama);

if(empty($namaproduk)){    //jika nama kosong maka muncul pesan
        $error="<p style='color:#F00;'>* Masukan Nama Produk</p>";
    }
     elseif(empty($harga)){ //jika harga kosong maka muncul pesan
        $error="<p style='color:#F00;'>* Masukan Harga Produk</p>";
    }
     elseif(empty($berat)){   //jika berat produk kosong maka muncul pesan
        $error="<p style='color:#F00;'>* Masukan Berat Produk dalam satuan gram</p>";
    }
    elseif(!is_numeric($berat)){  //jika berat bukan angka maka muncul pesan
        $error="<p style='color:#F00;'>* Masukan Berat Produk Dengan Angka</p>";
    }
    elseif(strlen($deskripsi) < 10){  //jika deskripsi kosong maka muncul pesan
        $error="<p style='color:#F00;'>* Masukan Deskripsi Produk minimal 200 karakter</p>";
    }
     else{ 



	$koneksi->query("INSERT INTO produk
		(nama_produk,harga_produk,berat_produk,foto_produk,deskripsi_produk)
		VALUES('$_POST[nama]','$_POST[harga]','$_POST[berat]','$nama','$_POST[deskripsi]')");

	echo "<div class='alert alert-info'>Data tersimpan</div>";
	echo "<meta http-equiv='refresh' content='1;url=index.php?halaman=produk'>";
 }
}


 ?>
