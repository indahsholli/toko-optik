<?php
session_start();
$koneksi = new mysqli("localhost","root","","optik");

if (!isset($_SESSION["pelanggan"]))
{
	echo "<script>alert('silahkan login');</script>";
	echo "<script>location='login.php';</script>";
}

?>
<!DOCTYPE html>
<html>
<head>
	<title>checkout</title>
	<link rel="stylesheet" href="admin/assets/css/bootstrap.css">
</head>
<body>

	<nav class="nav navbar-default">
	<div class="container">

	<ul class="nav navbar-nav">
		<li><a href="index.php">Home</a></li>
		<li><a href="keranjang.php">Keranjang</a></li>
		<?php if (isset($_SESSION["pelanggan"])): ?>
			<li><a href="logout.php">logout</a></li>
		<?php else: ?>
			<li><a href="login.php">login</a></li>
		<?php endif ?>

		<li><a href="checkout.php">Checkout</a></li>
	</ul>
</div>
</nav>

<section class="konten">
	<div class="container">
		<h1>Keranjang Belanja</h1>
		<hr>
		<table class="table table-bordered">
			<thead>
				<tr>
					<th>No</th>
					<th>Produk</th>
					<th>Harga</th>
					<th>Jumlah</th>
					<th>Subharga</th>
				</tr>
			</thead>
			<tbody>
				<?php $nomor=1; ?>
				<?php foreach ($_SESSION["keranjang"] as $id_produk => $jumlah): ?> 
				
				<?php 
				$ambil = $koneksi->query("SELECT * FROM Produk
					WHERE id_produk='$id_produk'");
				$pecah = $ambil->fetch_assoc();
				$subharga = $pecah["harga_produk"]*$jumlah; 
				?>
				<tr>
					<td><?php echo $nomor; ?></td>
					<td><?php echo $pecah["nama_produk"];?></td>
					<td>Rp.<?php echo number_format($pecah["harga_produk"]); ?></td>
					<td><?php echo $jumlah; ?></td>
					<td>Rp. <?php echo number_format($subharga); ?></td>
				</tr>
				<?php $nomor++; ?>
				<?php endforeach  ?>
			</tbody>
		</table>

		<form method="post">
			<div class="form-group">
				<input type="text" readonly value="<?php echo $_SESSION["pelanggan"]['nama']?>">
		</form>

	</div>
</section>

<pre><?php print_r($_SESSION['pelanggan']) ?></pre>


</body>
</html>